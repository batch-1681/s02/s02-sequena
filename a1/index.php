<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02: Repetition Control Structures and Array Manipulation</title>
</head>
<body>
    <h1>Divisibles of Five</h1>
    <p> <?php divisibleByFive(); ?></p>
  
    <h1>Array Manipulation</h1>
    <?php array_push($studentArray, 'John Smith') ?>
    

    <pre><?php print_r($studentArray) ?> </pre>
    <ul>
        <?php foreach($studentArray as $student) { ?>
            <li><?php echo $student ?></li>
        <?php } ?>
    </ul>

    <pre><?php echo count($studentArray) ?> </pre>

    <?php array_push($studentArray, 'Jane Doe') ?>

    <pre><?php print_r($studentArray) ?> </pre>
    <ul>
        <?php foreach($studentArray as $student) { ?>
            <li><?php echo $student ?></li>
        <?php } ?>
    </ul>
    <pre><?php echo count($studentArray) ?> </pre>

    <?php array_shift($studentArray) ?>
    <pre><?php print_r($studentArray) ?> </pre>
    <pre><?php echo count($studentArray) ?> </pre>

</body>
</html>